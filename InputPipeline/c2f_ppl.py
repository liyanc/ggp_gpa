"""
"""

__author__ = "Liyan Chen"


import cv2
import random
import numpy as np
import FileIO as fio
from tensorpack import *


def train_gen_imgf(ds_io):
    for imgid in ds_io.training_id:
        imgf = ds_io.get_imgfile(imgid)
        yield imgid, imgf


class TrainParallelReader:
    def __init__(self, ds_io):
        self.ds_io = ds_io

    def parallel_map(self, dp):
        imgid, imgf = dp
        vox_b_comp = self.ds_io.get_jointvox(imgid)
        img = np.ascontiguousarray(cv2.imread(imgf)[:, :, ::-1])
        return imgid, img, vox_b_comp


class SizedDataflowFromGenerator(dataflow.DataFromGenerator):
    def set_size(self, size):
        self.size = size

    def __len__(self):
        return self.size


def collect_vox(dp):
    imgid, img, vox_b_comp = dp
    voxes = fio.load_compressed_bytes(vox_b_comp)
    vox1, vox2, vox3, vox4 = [v.reshape([224, 224, -1]) for v in voxes]
    return imgid, img, vox1, vox2, vox3, vox4, True


def get_train_data(ds_io, bsize):
    train_mapper = TrainParallelReader(ds_io)
    in_df = SizedDataflowFromGenerator(train_gen_imgf(ds_io))
    in_df.set_size(len(ds_io.training_id))
    read_df = dataflow.MultiProcessMapDataZMQ(in_df, 4, train_mapper.parallel_map, strict=False, buffer_size=3)
    coll_df = dataflow.MapData(read_df, collect_vox)
    ins_q = input_source.BatchQueueInput(coll_df, bsize)
    ins_s = input_source.StagingInput(ins_q)
    return ins_q


def epoch_gen(ds_io):
    train_list = ds_io.training_id
    random.shuffle(train_list)
    for imgid in train_list:
        imgfile, joints, camproj = ds_io.get_imgfile_joint_cam(imgid, obj_cam=False)
        joints17 = np.ascontiguousarray(ds_io.select_joints(joints))
        yield imgid, imgfile, joints17, camproj


class VoxSpaceJointMapper:
    def __init__(self, ds_io, res_2d):
        self.ds_io = ds_io
        self.w, self.h = res_2d

    def voxelize_pts(self, joints, camproj, res_z):
        imgpts = self.ds_io.proj_imgpts(joints, camproj)
        voxel_z = self.ds_io.zoffset_to_voxel_z(joints, camproj, res_z)[:, np.newaxis]
        vox_pts = np.concatenate((imgpts, voxel_z), axis=1)
        return vox_pts

    def map2ground_truth(self, dp):
        imgid, imgfile, joints17, camparam = dp
        camproj = fio.param2camproj(camparam)
        img = cv2.imread(imgfile)
        voxp1, voxp2, voxp3, voxp4 = [self.voxelize_pts(joints17, camproj, res_z) for res_z in [1, 2, 4, 64]]
        return imgid, img, voxp1, voxp2, voxp3, voxp4, True


def get_train_data_voxpt(ds_io, bsize):
    mapper = VoxSpaceJointMapper(ds_io, (224, 224))
    in_df = SizedDataflowFromGenerator(epoch_gen(ds_io))
    in_df.set_size(len(ds_io.training_id))
    map_df = dataflow.MultiProcessMapDataZMQ(in_df, 10, mapper.map2ground_truth, strict=False)
    ins_q = input_source.BatchQueueInput(map_df, bsize)
    ins_s = input_source.StagingInput(ins_q)
    return ins_s
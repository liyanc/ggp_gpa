"""
"""

__author__ = "Liyan Chen"

import argparse
import os

import FileIO as fio
import Execution as exe
import InputPipeline as ippl
import Models as mdl

from tensorpack import logger
from tensorpack.callbacks import *
from tensorpack.train import *
from tensorpack.utils.gpu import get_num_gpu
from tensorpack.tfutils import SaverRestore

parser = argparse.ArgumentParser()
parser.add_argument('--gpu', help='comma separated list of GPU(s) to use.')
parser.add_argument("--sess")
parser.add_argument("--orig_ds")
parser.add_argument("--crop_ds")
parser.add_argument("--logdir")
parser.add_argument("--load")
args = parser.parse_args()


if args.gpu:
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

sess_rt = fio.concat_path(args.logdir, args.sess)
print("Session {:} root directory: {:}".format(args.sess, sess_rt))

exe.run_command(["mkdir", sess_rt])
exe.run_command(["mkdir", fio.concat_path(sess_rt, "checkpoints")])
exe.run_command(["mkdir", fio.concat_path(sess_rt, "log")])

logger.set_logger_dir(fio.concat_path(sess_rt, "log"))

print("Setup Dataset")
ds_io = fio.GPA1Dataset(args.orig_ds, args.crop_ds)
dataset_train = ippl.get_train_data_voxpt(ds_io, mdl.c2f_params.bsize)

print("Ready to train")
config = TrainConfig(
    model=mdl.C2FModel(mdl.c2f_config, mdl.c2f_params),
    data=dataset_train,
    callbacks=[
        ModelSaver(checkpoint_dir=fio.concat_path(sess_rt, "checkpoints"))
    ],
    max_epoch=4,
    session_init=SaverRestore(args.load) if args.load else None
)
num_gpu = max(get_num_gpu(), 1)
print("Use {:} GPUs to train".format(num_gpu))
launch_train_with_config(config, SyncMultiGPUTrainerParameterServer(num_gpu))

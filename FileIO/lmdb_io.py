"""
"""

__author__ = "Liyan Chen"

import lmdb
from .general_io import load_bytes, dump_bytes


class MetaIO:
    def __init__(self, path, readonly=True):
        self.db = lmdb.open(path, subdir=False, map_size=1099511627776 * 2, readonly=readonly, meminit=False,
                            map_async=True)
        self.count = 0
        self.readonly = readonly
        self.read_txn = self.db.begin(write=False)
        if not readonly:
            self.write_txn = self.db.begin(write=True)

    def get(self, imgid):
        if isinstance(imgid, int):
            k = u'{:0>10}'.format(imgid).encode('ascii')
        elif isinstance(imgid, bytes):
            k = imgid
        else:
            raise ValueError("imgid has to be bytes or int, not {:}".format(str(imgid)))

        with self.read_txn.cursor() as cur:
            return load_bytes(cur.get(k))

    def __iter__(self):
        with self.db.begin(write=False) as txn:
            with txn.cursor() as cur:
                for k, v in cur:
                    yield k, load_bytes(v)

    def put(self, imgid, v, write_frequency=500, ignore_readonly=False):
        if self.readonly and ignore_readonly:
            return False
        elif self.readonly and not ignore_readonly:
            raise ValueError("Current db is read-only")
        else:
            if isinstance(imgid, int):
                k = u'{:0>10}'.format(imgid).encode('ascii')
            elif isinstance(imgid, bytes):
                k = imgid
            else:
                raise ValueError("imgid has to be bytes or int, not {:}".format(str(imgid)))

            v_bytes = dump_bytes(v)
            res = self.write_txn.put(k, v_bytes)
            self.count += 1

            if self.count % write_frequency == 1:
                self.write_txn.commit()
                self.write_txn = self.db.begin(write=True)

            return res

    def finish_writing(self):
        self.read_txn.commit()
        self.read_txn = None
        self.write_txn.commit()
        self.write_txn = None
        self.db.sync()

    def close(self):
        if self.read_txn is not None:
            self.read_txn.commit()
        if self.write_txn is not None:
            self.write_txn.commit()
        self.db.sync()
        self.db.close()

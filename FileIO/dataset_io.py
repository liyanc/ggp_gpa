"""
"""

__author__ = "Liyan Chen"

import os
import numpy as np
import scipy.stats
from .general_io import *
from .lmdb_io import *
from .joint_name import *


class GPA1Dataset:
    def __init__(self, rt_original, rt_cropped, original_meta="new_meta.mdb", spc_meta="spc_meta.mdb", imgdir="img",
                 cropdir="cropped", jointvoxdir="train_vox"):
        self.rt_original = rt_original
        self.rt_cropped = rt_cropped
        self.img_orig = concat_path(rt_original, imgdir)
        self.img_crop = concat_path(rt_cropped, cropdir)
        self.vox_joint = concat_path(rt_cropped, jointvoxdir)
        self.meta_orig = MetaIO(concat_path(rt_cropped, original_meta))
        self.meta_spc = MetaIO(concat_path(rt_cropped, spc_meta))

        gpa1_id = [(row[0], row[-1]) for (_, row) in self.meta_orig if row[-2]]
        self.training_id = [imgid for (imgid, is_test) in gpa1_id if not is_test]
        self.testing_id = [imgid for (imgid, is_test) in gpa1_id if is_test]
        with open(concat_path(self.rt_cropped, "max_z_offset.txt"), "r") as f:
            self.max_z_offset = float(f.read())

    def get_imgfile(self, imgid, is_cropped=True):
        iid, subj, takename, cam, src_file, dst_file, cam_ts, mocap_ts, sess_date, cam_param_f, markers, joints, geo_name, is_gpa1, is_test = self.meta_orig.get(imgid)
        if is_cropped:
            return concat_path(self.img_crop, os.path.basename(dst_file.decode()))
        else:
            return concat_path(self.img_orig, os.path.basename(dst_file.decode()))

    def get_jointvox(self, imgid):
        voxfile = concat_path(self.vox_joint, u'{:0>10}.voxb'.format(imgid))
        return read_bytes(voxfile)

    def get_imgfile_joint_cam(self, imgid, is_cropped=True, obj_cam=True):
        """
        Use it only in the main process
        :param imgid:
        :param is_cropped:
        :param obj_cam: True if used directly; False if send through multiprocessing
        :return:
        """
        iid, subj, takename, cam, src_file, dst_file, cam_ts, mocap_ts, sess_date, cam_param_f, markers, joints, geo_name, is_gpa1, is_test = self.meta_orig.get(imgid)
        new_cam, scaling, c_offset = self.meta_spc.get(imgid)
        mm_cam = (new_cam[0], new_cam[1], new_cam[2], new_cam[3] * 10.0, new_cam[4])
        rtn_cam = param2camproj(mm_cam) if obj_cam else mm_cam

        # Convert from cm to mm
        joints = joints * 10.0
        if is_cropped:
            return concat_path(self.img_crop, os.path.basename(dst_file.decode())), joints, rtn_cam
        # TODO: change camera to the original camera
        else:
            return concat_path(self.img_orig, os.path.basename(dst_file.decode())), joints, rtn_cam

    def get_meta_new_cam(self, imgid, obj_cam=True):
        v = self.meta_orig.get(imgid)
        iid, subj, takename, cam, src_file, dst_file, cam_ts, mocap_ts, sess_date, cam_param_f, markers, joints, geo_name, is_gpa1, is_test = v
        new_cam, scaling, c_offset = self.meta_spc.get(imgid)
        mm_cam = (new_cam[0], new_cam[1], new_cam[2], new_cam[3] * 10.0, new_cam[4])
        rtn_cam = param2camproj(mm_cam) if obj_cam else mm_cam

        # Convert from cm to mm
        joints = joints * 10.0
        return (concat_path(self.img_crop, os.path.basename(dst_file.decode())), joints, rtn_cam), v

    def proj_cam_coordiante(self, joints, cam):
        return cam.camera_coordinate(joints)

    def select_joints(self, joints):
        return joints[ind_c, :]

    def z_offset(self, joints, cam):
        campts = cam.camera_coordinate(joints)
        rt_z = campts[2, rt_ind]
        z_offset = campts[2, :] - rt_z
        return z_offset, rt_z

    def proj_imgpts(self, joints, cam):
        return cam.project_linear(joints.T)

    def scaled_zoffset_by_imgid(self, joints, cam, scale):
        z_offset, _ = self.z_offset(joints, cam)
        return z_offset / self.max_z_offset * scale

    def recover_scaled_zoffset(self, z_offset, scale):
        return z_offset / scale * self.max_z_offset

    def zoffset_to_voxel_z(self, joints, cam, z_res):
        scale = (z_res - 1.0) / 2.0
        shift = (z_res - 1.0) / 2.0
        z_offset, _ = self.z_offset(joints, cam)
        return z_offset / self.max_z_offset * scale + shift

    def voxel_z_to_zoffset(self, z_offset, z_res):
        scale = (z_res - 1.0) / 2.0
        shift = (z_res - 1.0) / 2.0
        return (z_offset - shift) / scale * self.max_z_offset

    def combine_imgpts_zoffset(self, imgid, imgpts, zoffset, is_crop=True, root_depth=None):
        """
        Use it only in the main thread(process)
        :param imgid:
        :param imgpts:
        :param zoffset:
        :param is_crop:
        :param root_depth:
        :return:
        """
        imgfile, joints, cam = self.get_imgfile_joint_cam(imgid, is_crop)
        joint17 = self.select_joints(joints)

        # Case 1: Use ground truth root depth
        if root_depth is None:
            _, rt_z = self.z_offset(joint17, cam)
            z = zoffset + rt_z
            world_pts = cam.world_coordinate_by_z_imgpt(imgpts, z)
            return world_pts

        # Case 2: Use external root depth
        else:
            z = zoffset + root_depth
            world_pts = cam.world_coordinate_by_z_imgpt(imgpts, z)
            return world_pts

    def voxelize_imgpts_voxelz(self, imgpts, voxel_z, sigma, res_3d):
        joint_grid = []
        for joint_id in range(imgpts.shape[0]):
            imgpt = imgpts[joint_id, :]
            z_ind = voxel_z[joint_id]
            mu = (imgpt[0], imgpt[1], z_ind)
            grid = draw_3d_normal_numpy(res_3d, mu, sigma)
            joint_grid.append(grid)
        return joint_grid


def draw_3d_normal_numpy(N, mu, sigma, itval=2.6):
    sigma_x, sigma_y, sigma_z = np.array(sigma) * 1.0341304822335893
    N_x, N_y, N_z = N
    mu_x, mu_y, mu_z = np.array(mu) + 0.06

    lo_x, hi_x = max(0, int(mu_x - itval * sigma_x)), min(int(mu_x + itval * sigma_x), N_x - 1)
    lo_y, hi_y = max(0, int(mu_y - itval * sigma_y)), min(int(mu_y + itval * sigma_y), N_y - 1)
    lo_z, hi_z = max(0, int(mu_z - itval * sigma_z)), min(int(mu_z + itval * sigma_z), N_z - 1)

    ker_x = scipy.stats.multivariate_normal.pdf(
        np.linspace(lo_x, hi_x, hi_x - lo_x, endpoint=False),
        mean=mu_x, cov=sigma_x ** 2).reshape((-1, 1))
    ker_y = scipy.stats.multivariate_normal.pdf(
        np.linspace(lo_y, hi_y, hi_y - lo_y, endpoint=False),
        mean=mu_y, cov=sigma_y ** 2).reshape((1, -1))
    ker_z = scipy.stats.multivariate_normal.pdf(
        np.linspace(lo_z, hi_z, hi_z - lo_z, endpoint=False),
        mean=mu_z, cov=sigma_z ** 2).reshape((1, -1))

    ker = np.matmul(np.matmul(ker_x, ker_y)[..., np.newaxis], ker_z)
    ker /= np.sum(ker)

    grid = np.zeros((N_x, N_y, N_z), np.float32)
    grid[lo_x:hi_x, lo_y:hi_y, lo_z:hi_z] += ker
    return grid[:, :, :, np.newaxis].astype(np.float32)

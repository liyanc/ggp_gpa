"""
"""

__author__ = "Liyan Chen"

id2markers = ['Hips', 'Spine', 'Spine1', 'Spine2', 'Spine3', 'Neck', 'Head', 'Site', 'RightShoulder', 'RightArm',
              'RightForeArm', 'RightHand', 'RightHandEnd', 'Site', 'RightHandThumb1', 'Site', 'LeftShoulder', 'LeftArm',
              'LeftForeArm', 'LeftHand', 'LeftHandEnd', 'Site', 'LeftHandThumb1', 'Site', 'RightUpLeg', 'RightLeg',
              'RightFoot', 'RightToeBase', 'Site', 'LeftUpLeg', 'LeftLeg', 'LeftFoot', 'LeftToeBase', 'Site']

ind1 = [0, 24, 25, 26, 29, 30, 31, 2, 5, 6, 7, 17, 18, 19, 9, 10, 11]
ind2 = [3, 2, 1, 4, 5, 6, 0, 7, 8, 10, 16, 15, 14, 11, 12, 13, 9]
ind_c = [26, 25, 24, 29, 30, 31,  0,  2,  5,  7, 11, 10,  9, 17, 18, 19,  6]
rt_ind = 6

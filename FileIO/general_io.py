"""
"""

__author__ = "Liyan Chen"


import pickle
import Camera as camsolve
import msgpack
import msgpack_numpy
import zlib
msgpack_numpy.patch()


def load_pkl(fname, python2=False):
    encoding="latin1" if python2 else "ASCII"

    with open(fname, "rb") as f:
        return pickle.load(f, encoding=encoding)


def dump_pkl(obj, fname):
    with open(fname, "wb") as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


def write_bytes(buffer, fname):
    with open(fname, "wb") as f:
        f.write(buffer)


def read_bytes(fname):
    with open(fname, "rb") as f:
        return f.read()


def load_bytes(in_bytes):
    return msgpack.loads(in_bytes)


def dump_bytes(obj):
    return msgpack.dumps(obj, use_bin_type=True)


def dump_compressed_bytes(obj):
    return zlib.compress(msgpack.dumps(obj, use_bin_type=True), 9)


def load_compressed_bytes(obj):
    return msgpack.loads(zlib.decompress(obj))


def load_cam(camparam_file):
    cam_dict = {}
    param_dict = load_pkl(camparam_file)
    for cam, param in param_dict.items():
        csolver = camsolve.CameraSolverNonlinear()
        csolver.load_params(param)
        cam_dict[cam] = csolver
    return cam_dict


def dump_cam(cam_dict):
    return dict((k, v.dump_params) for k, v in cam_dict.items())


def param2camproj(params):
    camproj = camsolve.CameraSolverNonlinear()
    camproj.load_params(params)
    return camproj


def concat_path(*path):
    return "/".join(path)


class ArgPathBuilder:
    def __init__(self, args):
        self.args = args

    def __getattr__(self, item):
        return "{:}/{:}".format(self.args.root_dir, vars(self.args)[item])


"""
"""

__author__ = "Liyan Chen"

from .conv_units import *


def res_b(x, out_chan, is_training, name):
    if x.shape[3] == out_chan:
        return branch4_res_5x5(x, name, training=is_training)
    else:
        return branch4_avgpool_5x5(x, name, training=is_training, out_channel=out_chan)


def hourglass(x, n_stack, out_chan, is_training):
    l_name = "HgL{:}".format(n_stack)
    #print(l_name + " in_shape", x.shape)
    # Upper
    int_chann = 160 if out_chan < 200 else 256
    up1 = res_b(x, int_chann, is_training, l_name + "up1")
    up3 = res_b(up1, out_chan, is_training, l_name + "up3")

    # Lower
    pool = branch3_respool_keepsize_5x5(x, scope=l_name + "low1pool", training=is_training)

    low2 = res_b(pool, int_chann, is_training, l_name + "low2")

    if n_stack > 1:
        low4 = hourglass(low2, n_stack - 1, out_chan, is_training)
    else:
        low4 = res_b(low2, out_chan, is_training, l_name + "low4")
    low5 = res_b(low4, out_chan, is_training, l_name + "low5")

    #print(l_name + "lower_shape", low5.shape)
    # Back to upper
    up4 = tf.layers.conv2d_transpose(low5, out_chan, 3, 2, "same", name=l_name + "up4")
    return up3 + up4


def lin1x1(x, out_chan, name):
    return tf.layers.conv2d(x, out_chan, 1, padding='valid', name=name, activation=tf.nn.relu,
                            kernel_initializer=tf.contrib.layers.xavier_initializer_conv2d(uniform=False))


def convb1x1(x, out_chan, is_training, name):
    in_channel = x.shape[-1]
    with tf.variable_scope(name):
        return conv_bn_act(
            x, [1, 1, in_channel, out_chan], [1, 1], "_1x1",
            tf.contrib.layers.xavier_initializer_conv2d(uniform=False), is_training, 0.9, tf.nn.relu)
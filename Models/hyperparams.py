"""
"""

__author__ = "Liyan Chen"


class HyperParams:
    pass


c2f_params = HyperParams()
c2f_params.bsize = 2
c2f_params.lr = 5e-4

c2f_config = HyperParams()
c2f_config.img_size = (224, 224)
c2f_config.joints = 17
c2f_config.out_d = [1 * 17, 2 * 17, 4 * 17, 64 * 17]
c2f_config.pred_d = [1, 2, 4, 64]

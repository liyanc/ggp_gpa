"""
"""

__author__ = "Liyan Chen"


import NN as nn
import tensorflow as tf
import numpy as np
from tensorpack import *


class C2FModel(ModelDesc):
    def __init__(self, config, hparams):
        super(C2FModel, self).__init__()
        self.config = config
        self.hparams = hparams

    def main_c2f(self, img, t_flag_vec):
        t_flag = t_flag_vec[0]
        with tf.variable_scope("c2f"):
            with tf.variable_scope("stem"):
                r1 = nn.res_b(img, 64, t_flag, "r1")
                r2 = nn.branch3_respool_keepsize_5x5(
                    r1, scope="r2", training=t_flag)
                # r2 = nn.res_b(r1, 64, t_flag, "r2")
                r3 = nn.res_b(r2, 96, t_flag, "r3")

            with tf.variable_scope("HG1"):
                hg1 = nn.hourglass(r3, 4, 128, t_flag)

            with tf.variable_scope("out1"):
                l1 = nn.convb1x1(hg1, 64, t_flag, "l1")
                # l2 = nn.convb1x1(l1, 32, t_flag, "l2")
                # out1 = nn.lin1x1(l2, self.config.out_d[0], "out1")  # Out 1, 17 chan

                up_l2 = tf.layers.conv2d_transpose(l1, 32, 1, 2, "same", name="up_l2")
                concat_out1 = tf.concat([up_l2, r1], axis=3)
                out1 = nn.lin1x1(concat_out1, self.config.out_d[0], "out1")  # out 1, 17
                pout1 = tf.layers.conv2d(out1, 128, 1, (2, 2), 'same', name="pout1")

            with tf.variable_scope("int1"):
                concat1 = tf.concat([l1, r2], axis=3)
                lc1 = nn.lin1x1(concat1, 128, "lc1")
                int1 = pout1 + lc1

            with tf.variable_scope("HG2"):
                hg2 = nn.hourglass(int1, 4, 192, t_flag)

            with tf.variable_scope("out2"):
                l3 = nn.convb1x1(hg2, 128, t_flag, "l3")
                # l4 = nn.convb1x1(l3, 64, t_flag, "l4")
                # out2 = nn.lin1x1(l4, self.config.out_d[1], "out2") # Out 2, 34
                # pout2 = nn.lin1x1(out2, 160, "pout2")

                up_l4 = tf.layers.conv2d_transpose(l3, 64, 1, 2, "same", name="up_l4")
                concat_out2 = tf.concat([up_l4, r1], axis=3)
                out2 = nn.lin1x1(concat_out2, self.config.out_d[1], "out2")  # out 2, 34
                pout2 = tf.layers.conv2d(out2, 160, 1, (2, 2), 'same', name="pout2")

            with tf.variable_scope("int2"):
                concat2 = tf.concat([l3, l1], axis=3)
                lc2 = nn.lin1x1(concat2, 160, "lc2")
                int2 = pout2 + lc2

            with tf.variable_scope("HG3"):
                hg3 = nn.hourglass(int2, 4, 192, t_flag)

            with tf.variable_scope("out3"):
                l5 = nn.convb1x1(hg3, 160, t_flag, "l5")
                # l6 = nn.convb1x1(l5, 128, t_flag, "l6")
                # out3 = nn.lin1x1(l6, self.config.out_d[2], "out3") # Out 3, 68
                # pout3 = nn.lin1x1(out3, 192, "pout3")

                up_l6 = tf.layers.conv2d_transpose(l5, 128, 1, 2, "same", name="up_l6")
                concat_out3 = tf.concat([up_l6, r1], axis=3)
                out3 = nn.lin1x1(concat_out3, self.config.out_d[2], "out3")  # out 2, 34
                pout3 = tf.layers.conv2d(out3, 192, 1, (2, 2), 'same', name="pout3")

            with tf.variable_scope("int3"):
                concat3 = tf.concat([l5, l3], axis=3)
                lc3 = nn.lin1x1(concat3, 192, "lc3")
                int3 = pout3 + lc3

            with tf.variable_scope("HG4"):
                hg4 = nn.hourglass(int3, 4, 512, t_flag)

            with tf.variable_scope("out4"):
                l7 = tf.layers.conv2d_transpose(hg4, 512, 3, 2, "same", name="l7")
                # l7 = nn.convb1x1(hg4, 512, t_flag, "l7")
                concat_out4 = tf.concat([l7, r1], axis=3)
                l8 = nn.convb1x1(concat_out4, 512, t_flag, "l8")
                out4 = nn.lin1x1(l8, self.config.out_d[3], "out4") # out 4, 1088

        return out1, out2, out3, out4

    def read_pred(self, vox1, vox2, vox3, vox4):
        pred_pts = []
        for v, d in zip([vox1, vox2, vox3, vox4], self.config.pred_d):
            w, h = self.config.img_size
            x = np.linspace(0, w - 1, w)
            y = np.linspace(0, h - 1, h)
            z = np.linspace(0, d - 1, d)
            xv, yv, zv = np.meshgrid(x, y, z)

            vox = tf.reshape(v, [-1, w, h, d, self.config.joints])
            sum_mass = tf.reduce_sum(vox, axis=[1, 2, 3], keepdims=True)
            x_pred = tf.reduce_sum(xv[np.newaxis, :, :, :, np.newaxis] * vox / sum_mass, axis=[1, 2, 3], keepdims=False)
            y_pred = tf.reduce_sum(yv[np.newaxis, :, :, :, np.newaxis] * vox / sum_mass, axis=[1, 2, 3], keepdims=False)
            z_pred = tf.reduce_sum(zv[np.newaxis, :, :, :, np.newaxis] * vox / sum_mass, axis=[1, 2, 3], keepdims=False)
            pred = tf.concat([tf.expand_dims(t, 2) for t in [x_pred, y_pred, z_pred]], axis=2)
            pred_pts.append(pred)
            print("pred points shape", pred.shape)
        return pred_pts

    def loss(self, preds, labels):
        return [tf.losses.absolute_difference(l, pred) for (l, pred) in zip(labels, preds)]

    def build_graph(self, imgid, img, voxp1, voxp2, voxp3, voxp4, t_flag):
        self.imgid = imgid
        v1, v2, v3, v4 = self.main_c2f(img, t_flag)
        p1, p2, p3, p4 = self.read_pred(v1, v2, v3, v4)
        losses = self.loss([p1, p2, p3, p4], [voxp1, voxp2, voxp3, voxp4])
        self.l1, self.l2, self.l3, self.l4 = losses
        return tf.add_n(losses, name='cost')

    def optimizer(self):
        lr = tf.get_variable('learning_rate', initializer=self.hparams.lr, trainable=False)
        opt = tf.train.AdamOptimizer(lr)
        return opt

    def inputs(self):
        w, h = self.config.img_size
        d1, d2, d3, d4 = self.config.out_d
        return [
            tf.placeholder(tf.int32, (None,), 'imgid'),
            tf.placeholder(tf.float32, (None, w, h, 3), "img"),
            tf.placeholder(tf.float32, (None, self.config.joints, 3), "vox1"),
            tf.placeholder(tf.float32, (None, self.config.joints, 3), "vox2"),
            tf.placeholder(tf.float32, (None, self.config.joints, 3), "vox3"),
            tf.placeholder(tf.float32, (None, self.config.joints, 3), "vox4"),
            tf.placeholder(tf.bool, (None,), "t_flag")]

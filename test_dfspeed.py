"""
"""

__author__ = "Liyan Chen"

import FileIO as fio
import Execution as exe
import InputPipeline as ippl
import Models as mdl

from tensorpack import *

rt_crop = "/scratch/liyanc/GPA1.0"
rt_orig = "/scratch2/liyanc/GPA1.0"

ds_io = fio.GPA1Dataset(rt_orig, rt_crop)
dataset_train = ippl.get_train_data_voxpt(ds_io, mdl.c2f_params.bsize)
test_df = dataflow.TestDataSpeed(dataset_train)
test_df.start()